import { TestBed, async, inject } from '@angular/core/testing';
import { RegisterService } from '../service/registerService';
describe('Service: Company', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegisterService]
    });
  });

  it('should ...', inject([RegisterService], (service: RegisterService) => {
    expect(service).toBeTruthy();
  }));
});
