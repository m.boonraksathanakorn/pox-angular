import { Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})

export class RegisterService {
  
    constructor() { }
    
    validateAllFormFields(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }

  getDays():Number[]
  {
      const day =[];
      for (let index = 1; index <= 31; index++) {
      day.push(index);
      }
      return day;
  }
  
}