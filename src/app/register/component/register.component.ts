import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from'../service/registerService';
import { year } from'../model/years';
import { prefix } from '../model/prefix';
import { month } from '../model/month';
import { getLocaleDayNames } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  days :any;
  months : Array<string> =[
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฏาคม',
    'สิงหาคม',
    'กันยายน',
    'คุลาคม',
    'พฤศจิกายน',
    'ธันวาคม',
  ];
  prefixs: prefix[] = [
    {value: '1', viewValue: 'นาย'},
    {value: '2', viewValue: 'นาง'},
    {value: '3', viewValue: 'นางสาว'}
  ];
    
  years : year[] = [
    {value:2000},
    {value:2001},
    {value:2002},
    {value:2003},
    {value:2004},
    {value:2005},
    {value:2006},
    {value:2007},
  ]
  constructor(private formBuilder: FormBuilder , private registerService:RegisterService) { 
  
  }
  
  ngOnInit() :void{
    this.registerForm = this.formBuilder.group({
        prefix: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        citizenId: ['', Validators.required , Validators.minLength(13)],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        DOBType: ['', Validators.required],
        backCard: ['', Validators.required],
        day: ['', Validators.required],
        month: ['', Validators.required],
        year: ['', Validators.required],
    });

    this.days = this.registerService.getDays();

  }

  submitform(){
    if (this.registerForm.controls) {
      console.log('form submitted');
    } else {
      this.registerService.validateAllFormFields(this.registerForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {         
    Object.keys(this.registerForm.controls).forEach(field => {  
        const control = formGroup.get(field);             
        if (control instanceof FormControl) {             
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        
          this.validateAllFormFields(control);            
        }
    });
  }

  

}
