import { analyzeAndValidateNgModules } from '@angular/compiler';
import { findReadVarNames } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

interface prefix {
  value: string;
  viewValue: string;
}
interface year{
  value:number;
}
interface month{
  value:number;
  viewValue:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  registerForm: FormGroup;
    submitted = false;

  constructor(private formBuilder: FormBuilder) { }
  ngOnInit(){
    
  };
  title = 'poc-angular';
}
